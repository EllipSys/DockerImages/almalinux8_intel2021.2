FROM quay.io/almalinuxorg/almalinux:8

#RUN yum -y install epel-release
# make sure to install the correct release here, otherwise you run into GPG key errors
# RUN yum -y install http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-5.el7.noarch.rpm
# RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*
# 
# RUN yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
# RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*
# 
# RUN yum -y install https://harbottle.gitlab.io/epypel/7/x86_64/epypel-release.rpm
# RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*

RUN yum -y update ; yum clean all

RUN \
  yum -y install curl gcc vim python3 python3-pip python3-devel net-tools git
# yum -y remove git git-*
#RUN yum -y install \
#https://repo.ius.io/ius-release-el7.rpm \
#https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
#  yum -y install git224

RUN \
  yum -y install wget make cmake3 gcc-c++ && \
  yum clean all
RUN yum install https://rpm.nodesource.com/pub_20.x/nodistro/repo/nodesource-release-nodistro-1.noarch.rpm -y && \
  yum install nodejs -y --setopt=nodesource-nodejs.module_hotfixes=1 && \
  npm install --global yarn
RUN yum install -y wget unzip && \
  wget https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip && \
  unzip ninja-linux.zip && \
  mv ninja /usr/local/bin 

# Install Intel OneAPI 
RUN echo $'[oneAPI] \n\
name=Intel® oneAPI repository \n\
baseurl=https://yum.repos.intel.com/oneapi \n\
enabled=1  \n\
gpgcheck=0  \n\
repo_gpgcheck=0 \n\
gpgkey=https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB' > /etc/yum.repos.d/oneAPI.repo && \
rpm --import https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB

RUN yum install -y intel-oneapi-compiler-fortran-2021.2.0.x86_64 intel-oneapi-mpi-devel-2021.2.0.x86_64

RUN yum -y install libffi-devel bzip2-devel zlib-devel 
RUN yum -y install sqlite-devel readline-devel ncurses-devel xz-devel

#RUN wget https://www.openssl.org/source/openssl-1.1.1t.tar.gz && \
#tar xvf openssl-1.1.1t.tar.gz && \
#cd openssl-1.1*/ && \
#./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl && \
#make -j $(nproc) && \
#make install && \
#ldconfig
#ENV PATH /usr/local/openssl/bin:${PATH}
#ENV LD_LIBRARY_PATH /usr/local/openssl/lib:${LD_LIBRARY_PATH}
#ENV SSL_CERT_FILE /etc/ssl/certs/ca-bundle.crt

#RUN wget https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz && \
#    tar -xzf Python-3.9.7.tgz && \
#    cd Python-3.9.7 && \
#    ./configure --enable-optimizations && \
#    make altinstall
# RUN yum -y install makedepf90

# # Add gitlab-runner user
RUN yum install -y passwd &&\
  useradd gitlab-runner && \
  passwd -d gitlab-runner && \
  chown -R gitlab-runner:gitlab-runner /home/gitlab-runner

# ENV PYENV_ROOT=/home/gitlab-runner/.pyenv
# RUN curl https://pyenv.run | bash && \
# chmod -R a+rw /home/gitlab-runner/.pyenv && \
# echo 'export PYENV_ROOT="/home/gitlab-runner/.pyenv"' >> /home/gitlab-runner/.bashrc && \
# echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> /home/gitlab-runner/.bashrc

# Run script to set up shell environment for intel compilers on entry
COPY entrypoint.sh /root/entrypoint.sh
#RUN chown gitlab-runner:gitlab-runner /home/gitlab-runner/entrypoint.sh

# set ulimit -s
RUN ulimit -s unlimited
#RUN ln -s /usr/bin/cmake3 /usr/bin/cmake
RUN echo ulimit -s unlimited >> /home/gitlab-runner/.bashrc && \
#echo export INTEL_MKL_PATH=/opt/intel/oneapi/mkl/2021.2.0/lib/intel64/ >> /home/gitlab-runner/.bashrc && \
echo export LD_PRELOAD=\$INTEL_MKL_PATH/libmkl_core.so:\$INTEL_MKL_PATH/libmkl_intel_lp64.so:\$INTEL_MKL_PATH/libmkl_sequential.so >> /home/gitlab-runner/.bashrc
#RUN echo export PATH=/usr/local/openssl/bin:$\PATH >> /home/gitlab-runner/.bashrc && \
#echo export LD_LIBRARY_PATH=/usr/local/openssl/lib:$\LD_LIBRARY_PATH >> /home/gitlab-runner/.bashrc
RUN yum install -y procps
RUN yum install -y python39

#USER gitlab-runner
WORKDIR /home/gitlab-runner

# install Rust for gitlab-runner user
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

ENTRYPOINT ["/root/entrypoint.sh"]
CMD ["/bin/bash"]

